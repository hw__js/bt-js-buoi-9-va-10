function letInformationFromForm() {
  var employeeAccount = document.getElementById("tknv").value.trim();
  var employeeName = document.getElementById("name").value.trim();
  var employeeEmail = document.getElementById("email").value.trim();
  var employeePass = document.getElementById("password").value.trim();
  var employeeBirth = document.getElementById("datepicker").value.trim();
  var employeeSalary = document.getElementById("luongCB").value.trim();
  var employeePosition = document.getElementById("chucvu").value.trim();
  var hoursWorking = document.getElementById("gioLam").value.trim();

  var emp = new Employee(
    employeeAccount,
    employeeName,
    employeeEmail,
    employeePass,
    employeeBirth,
    employeeSalary,
    employeePosition,
    hoursWorking
  );
  return emp;
}

function renderEmployee(list) {
  var contentHTML = "";

  for (var i = 0; i < list.length; i++) {
    var currentEmp = list[i];

    var contentTr = `<tr>
        <td>${currentEmp.tk}</td>
        <td>${currentEmp.name}</td>
        <td>${currentEmp.email}</td>
        <td>${currentEmp.birth}</td>
        <td>${currentEmp.position}</td>
        <td>${currentEmp.calSalary()}</td>
        <td>${currentEmp.empRating()}</td>
        <td>
        <button class="btn btn-danger" onclick="deleteEmp('${
          currentEmp.tk
        }')">Xoa</button>
        <button class="btn btn-primary" onclick="editEmp('${
          currentEmp.tk
        }')" data-toggle="modal" data-target="#myModal">Sua</button>
        </td>
        </tr>`;

    contentHTML += contentTr;
  }
  document.getElementById("tableDanhSach").innerHTML = contentHTML;
}

function resetForm() {
  document.getElementById("empForm").reset();
  document.getElementById("tknv").disabled = false;
}

function showDataOnForm(emp) {
  document.getElementById("tknv").value = emp.tk;
  document.getElementById("name").value = emp.name;
  document.getElementById("email").value = emp.email;
  document.getElementById("password").value = emp.password;
  document.getElementById("datepicker").value = emp.birth;
  document.getElementById("luongCB").value = emp.salary;
  document.getElementById("chucvu").value = emp.position;
  document.getElementById("gioLam").value = emp.hours;
}
