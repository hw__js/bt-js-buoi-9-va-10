function isEmpty(value, idError) {
  if (value.length == 0) {
    document.getElementById(idError).innerText = "Please fill out this field.";
    return false;
  } else {
    document.getElementById(idError).innerText = "";
    return true;
  }
}

function checkExistedAccount(idAccount, listEmp, idError) {
  var index = listEmp.findIndex(function (emp) {
    return emp.tk == idAccount;
  });
  if (index == -1) {
    document.getElementById(idError).innerText = "";
    return true;
  } else {
    document.getElementById(idError).innerText = "This Account is existed.";
    return false;
  }
}

function checkAccount(value, idError) {
  const re = /^(?=.*[a-zA-Z])(?=.*\d*)([a-zA-Z]{4,6}|[a-zA-Z\d]{4,6})$/;

  var isAccount = re.test(value);
  if (!isAccount) {
    document.getElementById(idError).innerText = "Account is invalid";
    return false;
  } else {
    document.getElementById(idError).innerText = "";
    return true;
  }
}

function checkFullName(value, idError) {
  const re = /^[a-zA-Z]+(?:\s[a-zA-Z]+)$/;

  var iszFullName = re.test(value);
  if (!iszFullName) {
    document.getElementById(idError).innerText =
      "Full Name Must Not Digits or FirstName LastName";
    return false;
  } else {
    document.getElementById(idError).innerText = "";
    return true;
  }
}

function checkEmail(value, idError) {
  const re =
    /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;

  var isEmail = re.test(value);
  if (!isEmail) {
    document.getElementById(idError).innerText = "Invalid Email Format";
    return false;
  } else {
    document.getElementById(idError).innerText = "";
    return true;
  }
}

function checkPass(value, idError) {
  // Minimum 6 characters {>>6,10}
  // Maximum 10 characters {6,>>10}
  // At least one uppercase character (?=.*[A-Z])
  // At least one lowercase character (?=.*[a-z])
  // At least one digit (?=.*\d)
  // At least one special character (?=.*[a-zA-Z >>!#$%&? "<<])[a-zA-Z0-9 >>!#$%&?<< ]

  const re =
    /^(?=.*\d)(?=.*[A-Z])(?=.*[a-z])(?=.*[a-zA-Z!#$%&? "])[a-zA-Z0-9!#$%&?]{6,10}$/;

  var isPass = re.test(value);
  if (!isPass) {
    document.getElementById(idError).innerText = "Invalid Password Format";
    return false;
  } else {
    document.getElementById(idError).innerText =
      "Hurray! Your Password is Valid and Strong.";
    return true;
  }
}

function checkDate(value, idError) {
  const re = /^(0[1-9]|1[0-2])\/(0[1-9]|1\d|2\d|3[01])\/(19|20)\d{2}$/;

  var isDate = re.test(value);
  if (!isDate) {
    document.getElementById(idError).innerText = "Invalid Date Format";
    return false;
  } else {
    document.getElementById(idError).innerText = "";
    return true;
  }
}

function checkSalary(value, idError) {
  if (value >= 1000000 && value <= 20000000) {
    document.getElementById(idError).innerText = "";
    return true;
  } else {
    document.getElementById(idError).innerText =
      "Salary must from 1 000 000 to 20 000 000";
    return false;
  }
}

function checkPosition(value, idError) {
  if (value == "Nhân viên" || value == "Trưởng phòng" || value == "Sếp") {
    document.getElementById(idError).innerText = "";
    return true;
  } else {
    document.getElementById(idError).innerText = "Please choose position";
    return false;
  }
}

function checkWorkingTime(value, idError) {
  if (value >= 80 && value <= 200) {
    document.getElementById(idError).innerText = "";
    return true;
  } else {
    document.getElementById(idError).innerText =
      "Working time must be between 80 and 200 hours.";
    return false;
  }
}
