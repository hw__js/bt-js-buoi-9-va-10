var empList = [];
const EMPLIST = "LIST";

// get data from LocalStorage
var dataJson = localStorage.getItem(EMPLIST);
if (dataJson) {
  var rawData = JSON.parse(dataJson);

  empList = rawData.map(function (item) {
    return new Employee(
      item.tk,
      item.name,
      item.email,
      item.password,
      item.birth,
      item.salary,
      item.position,
      item.hours
    );
  });
  renderEmployee(empList);
}

function saveLocalStorage() {
  var listJson = JSON.stringify(empList);
  localStorage.setItem(EMPLIST, listJson);
}

// Function to add new employee
document.getElementById("btnThemNV").onclick = function () {
  var newEmp = letInformationFromForm();
  var isValid = true;

  isValid &=
    isEmpty(newEmp.tk, "tbTKNV") &&
    checkAccount(newEmp.tk, "tbTKNV") &&
    checkExistedAccount(newEmp.tk, empList, "tbTKNV");

  isValid &=
    isEmpty(newEmp.name, "tbTen") && checkFullName(newEmp.name, "tbTen");

  isValid &=
    isEmpty(newEmp.email, "tbEmail") && checkEmail(newEmp.email, "tbEmail");

  isValid &=
    isEmpty(newEmp.password, "tbMatKhau") &&
    checkPass(newEmp.password, "tbMatKhau");

  isValid &=
    isEmpty(newEmp.birth, "tbNgay") && checkDate(newEmp.birth, "tbNgay");

  isValid &=
    isEmpty(newEmp.salary, "tbLuongCB") &&
    checkSalary(newEmp.salary, "tbLuongCB");

  isValid &= checkPosition(newEmp.position, "tbChucVu");

  isValid &=
    isEmpty(newEmp.hours, "tbGiolam") &&
    checkWorkingTime(newEmp.hours, "tbGiolam");

  if (isValid) {
    empList.push(newEmp);

    // save local
    saveLocalStorage();
    renderEmployee(empList);
    resetForm();
  }
};

// function to delete employee'account
function deleteEmp(tkEmp) {
  var index = empList.findIndex(function (emp) {
    return emp.tk == tkEmp;
  });
  if (index == -1) return;

  // delete the account of employee
  empList.splice(index, 1);
  console.log(" empList: ", empList);
  // update form after deleting account
  renderEmployee(empList);

  // store data to local storage when an employee account is deleted
  saveLocalStorage();
}

function editEmp(tkEmp) {
  var index = empList.findIndex(function (emp) {
    return emp.tk == tkEmp;
  });
  if (index == -1) return;

  var emp = empList[index];
  showDataOnForm(emp);
  document.getElementById("tknv").disabled = true;
}

// function to update account'information of employee
document.getElementById("btnCapNhat").onclick = function () {
  var editEmp = letInformationFromForm();

  var index = empList.findIndex(function (emp) {
    return emp.tk == editEmp.tk;
  });
  if (index == -1) return;

  var isValid = true;

  isValid &=
    isEmpty(editEmp.name, "tbTen") && checkFullName(editEmp.name, "tbTen");

  isValid &=
    isEmpty(editEmp.email, "tbEmail") && checkEmail(editEmp.email, "tbEmail");

  isValid &=
    isEmpty(editEmp.password, "tbMatKhau") &&
    checkPass(editEmp.password, "tbMatKhau");

  isValid &=
    isEmpty(editEmp.birth, "tbNgay") && checkDate(editEmp.birth, "tbNgay");

  isValid &=
    isEmpty(editEmp.salary, "tbLuongCB") &&
    checkSalary(editEmp.salary, "tbLuongCB");

  isValid &= checkPosition(editEmp.position, "tbChucVu");

  isValid &=
    isEmpty(editEmp.hours, "tbGiolam") &&
    checkWorkingTime(editEmp.hours, "tbGiolam");

  if (isValid) {
    empList[index] = editEmp;
    saveLocalStorage();
    renderEmployee(empList);
    resetForm();
  }
};

function searchEmp(value, listEmp) {
  var filteredData = [];

  for (var i = 0; i < listEmp.length; i++) {
    value = value.toLowerCase();
    var typeOfEmp = listEmp[i].empRating().toLowerCase();
    if (typeOfEmp.includes(value)) {
      filteredData.push(listEmp[i]);
    }
  }
  return filteredData;
}

document.getElementById("btnTimNV").onclick = function () {
  var letValue = document.getElementById("searchName").value.trim();
  console.log("letValue: ", letValue);

  var searchPosition = searchEmp(letValue, empList);
  renderEmployee(searchPosition);
};
